# Curso Terraform

## Criando os primeiros recursos

- Criação de uma máquina
- Criando um security group
- Associando o security group para liberar a porta TCP 22 (SSH) a máquina

Foram usados até aqui:

- terraform init
- terraform plan
- terraform show
- terraform apply