provider "aws" {
  version = "~> 2.0"
  region = "us-east-1"
}

resource "aws_instance" "development" {
  count         = 1                       # Quantidade de máquinas a serem criadas.
  ami           = "ami-07ebfd5b3428b6f4d" # Ubuntu Server 18.04 LTS (HVM), SSD Volume Type 
  instance_type = "t2.micro"              # Tipo tamanho
  key_name      = "tuckers"               # Chave para SSH
  tags = {
      Name = "development-${count.index}" # Padrão de nome da máquina se subir a mais que uma será development-$N
  }
}

resource "aws_security_group" "acesso_ssh" {
  name = "acesso_ssh"
  description =  "Permite acesso SSH as maquinas"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"] # Unsafe
  }
  tags = {
    Name = "sg_acesso_ssh"
  }
}
